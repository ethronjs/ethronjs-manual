# Introducción a Ethron

*Tiempo estimado de lectura: 6min*

**Ethron** es una suite de IT que presenta los siguientes componentes:

- **Ethron.js**, una plataforma de automatización IT con soporte para blockchain.
  Objeto de este manual.

- **EthronWS**, una plataforma de aplicaciones HTTP y de colas con soporte para **PostgreSQL**, **Redis**, **ZeroMQ** y **blockchain**.

- **EthronDB**, una base de datos de blockchain.

- **EthronPM**, una empaquetador de plugins, generadores y catálogos de **Ethron.js**, así como **contratos de blockchain**.

## Ethron.js

**Ethron.js** es una plataforma de automatización IT que proporciona varias funcionalidades bajo una API sencilla, reduciendo así la curva de aprendizaje.
Groso modo: **Ethron.js = Grunt + Mocha + Yeoman + Chai + Truffle**, pero bajo un enfoque diferente y más sencillo.
Presenta las siguientes características:

- Es **fácil de aprender y de usar**.
- Proporciona  un **motor de tareas** igual que hacen otras herramientas de automatización como *Grunt*, *Gulp* y *Puppet*.
- Proporciona distintos tipos de de tareas para **adaptarse a las necesidades de los usuarios** como, p.ej., tareas simples, macros y flujos de trabajo.
- Proporciona un **framework de pruebas de software** al igual que hacen *Jasmine*, *Mocha* y *Jest*.
- Proporciona una **biblioteca de aserciones** al igual que *Should* y *Chai*.
- Proporciona **generadores** al igual que *Yeoman*.
- Se puede **extender su funcionalidad** mediante **JavaScript** y **Dogma** usando **plugins**, **catálogos** y **generadores**.
- Facilita el **desarrollo de aplicaciones descentralizadas** (DApss) con **blockchain** tal como también permite **Truffle** y **Embark**.

En el *roadmap* del proyecto, tenemos las siguientes características en mente:

- Soporte integrado para **SSH**, lo que permitirá ejecutar tareas en **máquinas remotas** tal como hacen **Ansible** y **Salt**.
- Soporte de tareas de **colas**, lo que permitirá enviar y recibir mensajes de colas **Redis**, **ZeroMQ** y **PostgreSQL**.
- Notificación de la ejecución de tareas a colas para su consumo, p. ej., para su presentación o almacenamiento en bases de datos.
- Añadidura de nuevas redes de blockchain como **Parity** y **Geth**.
- Incremento de generadores, plugins y catálogos reutilizables.
- Consola web desde la que lanzar la ejecución de tareas y recibir notificación de su evolución y resultado.
- Soporte para el **navegador**.
- Realización de las pruebas para permitir su uso en **Windows** y **Raspberry Pi**.

Aunque puede utilizarse en varios ámbitos, está dirigido principalmente a:

- **Desarrolladores de software** de todo tipo de aplicaciones como, p.ej., **Node.js** o **blockchain**.
- **Administradores de sistemas**.
- **DevOps**.
- **Administradores de bases de datos**.

### Requisitos

Es indispensable tener conocimiento de **JavaScript** y **Node.js**.
Si además va a desarrollar aplicaciones sobre **Ethereum**, podrá usar **Vyper** y/o **Solidity**.

## Qué es la automatización TI

La **automatización** (*automation*) no es más que el proceso mediante el cual se convierte tareas manuales repetitivas en automáticas.
Al automatizarlas, podemos ejecutarlas más fácil y rápidamente que si tuviéramos que hacerlo una y otra vez manualmente.
Por su parte, la **automatización TI** (*IT automation*) es aquella que se centra en automatizar tareas y/o procesos de TI.

Entre los principales usos de la automatización TI, encontramos:

- Construcción de software.
- Despliegue o instalación de software.
- Realización de pruebas.
- Administración y monitorización de configuración.
- Realización de empaquetados.
- Extracción, transformación y carga de datos.
- Generación de archivos y/o directorios a partir de plantillas.

Y entre las principales ventajas:

- Aumento de la productividad.
- Aumento de la calidad.
- Aumento de la precisión o exactitud.
- Aumento de la robustez.
- Aumento de la reutilización.
- Reducción de costes.
- Reducción de errores.
- Reducción de riesgos.
- Reducción de tareas manuales.
- Reducción de tiempos de ejecución, de operación, de despliegue...
- Reducción de mantenimiento.
- Reducción de la duplicidad de trabajos.
- Reducción de la complejidad.

**Ethron.js**, al igual que **EthronWS** y **EthronPM**, se ha desarrollado en [Dogma](http://dogmalang.com).
Y se ha compilado a **JavaScript**, conociéndose esta edición formalmente como **Ethron.js**.
A lo largo de 2020, se publicará **Ethron.py**, una edición para **Python**.
Así, se proporcionará la misma API y la misma forma de trabajo en las dos plataformas de *scripting* más conocidas y utilizadas hoy en día.
Facilitando considerablemente la migración de una plataforma a otra con menor esfuerzo y menor curva de aprendizaje.
No siendo necesario aprender otra herramienta de automatización TI por el mero hecho de cambiar de Python a Node.js o viceversa.

## Arquitectura

La **arquitectura** (*architecture*) describe el conjunto y la estructura de componentes de algo, en nuestro caso, de **Ethron.js**.
Tenemos básicamente los siguientes elementos:

- Las **tareas**, las cuales representan trabajos o actividades a ejecutar como, p.ej., una operación de compilación, de despliegue, etc.
- El **motor de tareas**, el programa que ejecuta y supervisa las tareas.
- El **informador de resultados**, aquel que informa de las tareas en ejecución, mostrando el tiempo que tardaron y su estado final.
  P.ej., a través de la consola o de una web.
- Los **catálogos de tareas**, contenedores de tareas empaquetadas para su ejecución desde línea de comandos o una consola web.
- Los **plugins**, los cuales proporcionan tareas reutilizables como, p.ej., para trabajar con archivos, compilar, desplegar, etc.
- Los **generadores**, componentes que permiten crear directorios y archivos como, p.ej., para iniciar un nuevo proyecto.

A continuación, se muestra la salida de `ethonjs` para la lista de tareas de un catálogo:

```
$ ethronjs -c verdaccio

 Id.      Group  Type      Desc.                      
------------------------------------------------------
 console         Simple()  Open Web console           
 dflt            Macro     Start Verdaccio container  
 lint            Macro     Check source code          
 pull            Simple()  Pull Verdaccio image       
 rm              Simple()  Remove Verdaccio container
 run             Macro     Run Verdaccio container    
 start           Macro     Start Verdaccio container  
 stop            Simple()  Stop Verdaccio container   

$
```

Y a continuación, un ejemplo de ejecución de la tarea `run` del catálogo anterior:

```
$ ethronjs -c verdaccio r run

run
---
Run Verdaccio container
  Exec 'sudo rm -rf /home/me/opt/verdaccio' OK (8 ms)
  Create dir '/home/me/opt/verdaccio' OK (1 ms)
  Copy /home/me/tests/verdaccio/config.yaml to /home/me/opt/verdaccio/conf/config.yaml OK (2 ms)
  Create dir '/home/me/opt/verdaccio/storage' OK (0 ms)
  Create dir '/home/me/opt/verdaccio/plugins' OK (0 ms)
  Exec 'sudo chown -R 100:101 /home/me/opt/verdaccio' OK (6 ms)
  Verdaccio: run container 'verdaccio' (verdaccio/verdaccio) OK (2604 ms)
  Open Web console OK (6 ms)

 OK       8
 FAILED   0
 TOTAL    8

 2640 ms

$
```

Cuando se diseñó **Ethron.js**, se buscó tener un enfoque similar a programar *scripts* sencillos en **JavaScript**.
He aquí la definición del catálogo del ejemplo anterior:

```
//imports
const {cat} = require("ethron");
const docker = require("@ethronjs/plugin.docker");
const eslint = require("@ethronjs/plugin.eslint");
const exec = require("@ethronjs/plugin.exec");
const fs = require("@ethronjs/plugin.fs");
const verdaccio = require("@ethronjs/plugin.verdaccio");
const xdg = require("@ethronjs/plugin.xdg");
const path = require("path");
const fsx = require("fs-extra");

//Verdaccio container home.
const home = path.join(process.env.HOME, "opt/verdaccio");

//Image to use.
const image = "verdaccio/verdaccio";

//Container name.
const container = "verdaccio";

//Use sudo?
const sudo = true;

//Time to wait.
const sleep = "2s";

//Config file to use.
const config = (
  fsx.existsSync(path.join(process.cwd(), "config.yaml")) ?
    path.join(process.cwd(), "config.yaml") :
    path.join(__dirname, "tmpl/config.yaml")
);

//catalog
cat.macro("lint", [
  [eslint, "."]
]).title("Check source code");

cat.call("console", xdg.open, {
  target: "http://localhost:4873"
}).title("Open Web console");

cat.call("pull", docker.pull, {
  image
}).title("Pull Verdaccio image");

cat.macro("run", [
  [exec, `sudo rm -rf ${home}`],
  [fs.mkdir, home],
  [fs.cp, config, path.join(home, "conf", "config.yaml")],
  [fs.mkdir, path.join(home, "storage")],
  [fs.mkdir, path.join(home, "plugins")],
  [exec, `sudo chown -R 100:101 ${home}`],
  [verdaccio.run, {home, image, container, publish: 4873, sudo, sleep}],
  cat.get("console")
]).title("Run Verdaccio container");

cat.macro("start", [
  [verdaccio.start, {container, sudo, sleep}],
  cat.get("console")
]).title("Start Verdaccio container");

cat.call("stop", verdaccio.stop, {
  container,
  sudo
}).title("Stop Verdaccio container");

cat.call("rm", docker.rm, {
  container,
}).title("Remove Verdaccio container");

cat.macro("dflt", [
  cat.get("start")
]).title("Start Verdaccio container");
```
