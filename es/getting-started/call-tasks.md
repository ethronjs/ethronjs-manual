# Tareas de llamadas

*Tiempo estimado de lectura: 1min*

Una **tarea de llamada** (*call task*), o simplemente **llamada** (*call*), es un tipo especial de tarea que representa una invocación a una determinada tarea con unos determinados parámetros.
Cada vez que se invoque la tarea, se invocará su tarea adjunta con los parámetros indicados en su definición.
Son muy utilizadas.

## Definición de llamadas

Para definir una llamada, se utiliza la función `call()` del paquete `ethron` o `cat.call()`:

```
call(props, task, params) : Call
```

El parámetro `props` es similar al presentado en las tareas simples, macros y flujos de trabajo.
Mediante el parámetro `task` se indica la tarea a invocar.
Y con `params`, los parámetros a pasarle a la tarea a invocar.

Vamos a ver un ejemplo ilustrativo:

```
cat.call("pub", npm.publish, {
  who,
  access: "public",
  path: `dist/${pkg}`
}).title("Publish on NPM");
```
