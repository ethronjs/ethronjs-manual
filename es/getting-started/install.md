# Instalación de NPM

*Tiempo estimado de lectura: 2min*

**Ethron.js** está desarrollado en [Dogma](http://dogmalang.com) y compilado a **JavaScript** para **Node.js** y, próximamente, también el navegador.
Actualmente, las pruebas se han realizado sobre **Ubuntu**.
En un futuro cercano, se espera probar la aplicación en **Windows** y **Raspberry Pi**.

## Dependencias

**Node.js** y **NPM**.

## Instalación de Ethron.js

Una vez instaladas las dependencias, lo siguiente es conocer los paquetes NPM básicos:

- `ethron`, contiene el *kernel* de **Ethron.js**.
  Se recomienda una instalación local para cada proyecto.

- `ethronjs`, contiene la interfaz de línea de comandos de **Ethron.js**.

Además, se recomienda instalar los generadores básicos.

Una buena instalación inicial puede consistir en ejecutar lo siguiente:

```
$ npm i -g --production ethronjs
$ npm i -g --production @ethrongen/cat
```

Una vez instalado, es buena práctica comprobar que se tiene acceso al comando `ethronjs` y al generador `cat`:

```
$ ethronjs help
$ ethronjs g cat -h
```
