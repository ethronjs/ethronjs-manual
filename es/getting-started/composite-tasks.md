# Tareas compuestas

*Tiempo estimado de lectura: 5min*

Recordemos que las tareas se clasifican en simples, compuestas y de llamada.
Las simples son aquellas que son indivisibles y representan la unidad más pequeña de trabajo.
Por su parte, una **tarea compuesta** (*composite task*) es aquella que está formada por cero, una o más tareas.
Se clasifican en macros y flujos de trabajo.

## Macros

Una **macro** es una secuencia ordenada de cero, una o más tareas.
Cada vez que se invoca la macro, se ejecuta cada una de sus tareas en el orden indicado.

### Definición de macros

Para definir una macro, se utiliza la función `macro()` del paquete `ethron` o `cat.macro()` si estamos catalogándola:

```
macro(props, tasks:object[]) : Macro
```

El parámetro `props` es similar al presentado en las tareas simples.
Mientras que el parámetro `tasks` consiste en una lista o *array* de las tareas asociadas a la macro.
He aquí un ejemplo ilustrativo:

```
cat.macro("build", [
  cat.get("lint"),
  cat.get("trans-dogma"),
  cat.get("trans-js"),
  [fs.cp, "package.json", `dist/${pkg}/package.json`],
  [fs.cp, "README.md", `dist/${pkg}/README.md`],
  [fs.cp, "tmpl", `dist/${pkg}/tmpl`],
  [fs.cp, "test/data", `dist/${pkg}/test/data`]
]).title("Build package");
```

Cada tarea se puede especificar de varias maneras distintas.
Por un lado, mediante la propia tarea como en el caso de `cat.get()` que devuelve una tarea ya catalogada.

Por otro lado, podemos indicar un objeto que contiene la información sobre la invocación de la tarea:

- `title` (string). Contiene el título de la invocación de la tarea. Se usa en el informe final.
- `task` (Task, obligatorio). Contiene la tarea a invocar.
- `params` (object). Contiene los parámetros a pasar a la tarea cuando la invoque la macro.

Ejemplo:

```
cat.macro("lint", [
  {title: "Check Dogma code", task: exec, params: {cmd: "dogmac lint -l src test"}},
  {title: "Check JavaScript code", task: eslint, params: {src: "."}}
]).title("Lint source code");
```

La sintaxis final es mediante una lista o array en la que el primer elemento es la tarea a ejecutar y el resto sus parámetros.
Esta sintaxis sólo es posible cuando la tarea indicada presenta la función `fmtParams()`.
Ejemplo:

```
[fs.cp, "package.json", `dist/${pkg}/package.json`]
```

## Macros diferidas

Una **macro diferida** (*deferred macro*) es aquella cuyas tareas asociadas se determinan cuando se invoca por primera vez.
Estas tareas a su vez proceden de un directorio, donde cada uno de sus archivos representa un módulo, el cual debe exportar una definición de tarea.

### Definición de macros diferidas

Para definir una macro diferida, se utiliza la función `macro()`, pero ahora se pasa el directorio en el que se encuentran sus tareas, en vez de una lista de tareas:

```
macro(props, dir:string) : DeferredMacro
```

Ejemplo:

```
cat.macro("test", `./dist/${pkg}/test/unit`).title("Unit testing");
```

Cuando el directorio comienza por punto (`.`) o por dos puntos (`..`), la macro asume que es relativo al directorio actual de trabajo del proceso.
Lo anterior es lo mismo que:

```
cat.macro("test", path.join(process.cwd(), `dist/${pkg}/test/unit`)).title("Unit tests");
```

## Flujos de trabajo

Un **flujo de trabajo** (*workflow*) es una tarea compuesta que permite ejecutar unas tareas u otras atendiendo a un determinado flujo de ejecución.
En **Ethon.js**, este flujo se define mediante una función JavaScript, donde se puede usar sentencias `if`, `while`, `for` o cualquiera otra y donde se puede invocar cualquier tarea.
Cada tarea invocada dentro del flujo de trabajo se asociará a él y así se mostrará en el informe final.

Observe que a diferencia de las macros, donde sus tareas se invocan una detrás de otra, según el orden en el que se indican, en un flujo de trabajo las tareas se invocan cuando es necesario y si es necesario.

### Definición de flujos de trabajo

Para definir un flujo de trabajo, se usa la función `workflow()` del paquete `ethron` o `cat.workflow()`:

```
macro(props, fn) : Workflow
```

El parámetro `props` es similar al presentado en las tareas simples y macros.
Mientras que el parámetro `fn` consiste en la función de tarea que describe el flujo de trabajo.
Esta función puede tener varios parámetros:

- `params` (object). Parámetros pasados a la tarea, en la invocación del usuario.
- `done` (function). Función que debe invocarse para indicar que la tarea asíncrona ha finalizado.
- `log` (function). Función a través de la cual escribir mensajes de log en la consola.

No olvide que, en **Ethron.js**, los parámetros son nominales y no posicionales.
Respete los nombres indicados y ubique los parámetros donde mejor le venga.

Y al igual que las funciones de las tareas simples, una función asíncrona se puede definir mediante `async` o el parámetro `done`.
Use lo que más le convenga.

He aquí un ejemplo con el que detener varios contenedores *Docker* mediante un flujo de trabajo:

```
stop = workflow("stop", async function() {
  for (let container of ["contenedor1", "contenedor2"]) {
    await(docker.stop({container}));
  }
}).title("Stop the Docker containers");
```
