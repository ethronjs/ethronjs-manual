# Tareas simples

*Tiempo estimado de lectura: 6min*

Una **tarea** (*task*) representa un trabajo o actividad a ejecutar como, p.ej.:

- Una operación de compilación.
- La minimización de código JavaScript, HTML o CSS.
- La realización de una copia de seguridad.
- El alta de un nuevo usuario.
- La ejecución de un administrador de paquetes como, p.ej., APT, APK, NPM o LuaRocks.
- El inicio de uno o más contenedores de Docker.
- El inicio de una instancia de base de datos local.

Las tareas se clasifican en simples, compuestas y de llamada, siendo las primeras el objeto de este capítulo.

Una **tarea simple** (*simple task*) es la unidad de operación más pequeña que tiene como objeto realizar un determinado trabajo indivisible.
En **Ethron.js**, se implementa mediante una función **JavaScript**.
Su ejecución la lanza el ejecutor de tareas, el cual recopilará información sobre su duración y resultado final.
A medida que el ejecutor ejecuta una tarea, va generando notificaciones que pueden procesar los informadores para mostrar la evolución al usuario o incluso registrarla en una base de datos.
De manera predeterminada, se usa el informador de consola que muestra cuándo comienza una tarea, cuánto ha tardado y cómo ha finalizado.
Para no perder el foco, recordemos la ejecución de dos tareas de un catálogo NPM:

```
$ ethronjs -c @ethroncat/verdaccio r pull run

pull
----
Pull Verdaccio image OK (25002 ms)

 OK       1
 FAILED   0
 TOTAL    1

 25012 ms


run
---
Run Verdaccio container
  Exec 'sudo rm -rf /home/me/.verdaccio' OK (15 ms)
  Create dir '/home/me/.verdaccio' OK (1 ms)
  Copy /home/me/opt/verdaccio/config.yaml to /home/me/.verdaccio/conf/config.yaml OK (2 ms)
  Create dir '/home/me/.verdaccio/storage' OK (0 ms)
  Create dir '/home/me/.verdaccio/plugins' OK (0 ms)
  Exec 'sudo chown -R 100:101 /home/me/.verdaccio' OK (7 ms)
  Verdaccio: run container 'verdaccio' (verdaccio/verdaccio:latest) OK (2594 ms)
  Open Web console OK (6 ms)

 OK       8
 FAILED   0
 TOTAL    8

 2635 ms

$
```

## Definición de tareas simples

Una tarea simple se define mediante la función `simple()` del paquete `ethron` o, si estamos definiendo una tarea catalogada, mediante `cat.simple()`:

```
function simple(props, fn:function) : simple
```

El parámetro `props` puede ser un texto que indica el identificador de la tarea o bien un objeto con las propiedades de la tarea:

- `id` (string, obligatorio). Identificador de la tarea.
- `title` (string). Título predeterinado de la tarea que se utilizará en el informe presentado al usuario.
- `fmt` (function). Función que utilizará el motor de tareas para obtener el título dínamicamente: `function(params) : string`.
- `fmtParams` (function). Función que utilizará el motor de tareas para obtener el objeto de parámetros a pasar a la función de tarea: `function(params) : object`.
- `desc` (string). Descripción de la tarea.
- `tags` (string). Etiquetas asociadas a la tarea.
  Se utilizan para filtrar tareas a ejecutar.
- `delay` (number). Indica cuánto tiempo, en milisegundos, debe esperar el motor de tareas antes de comenzar su ejecución.

Mientras que el parámetro `fn` contiene la **función de tarea** (*task function*), aquella que contiene la lógica que realiza el trabajo asociado a la tarea.
Esta función puede tener varios parámetros:

- `params` (object). Parámetros pasados a la tarea, en la invocación del usuario.
  Las tareas que presentan este parámetro se conocen formalmente como **tareas parametrizadas** (*parameterized tasks*).
- `done` (function). Función que debe invocar la tarea para indicar que ha finalizado.
  Las tareas que presentan este parámetro se conocen formalmente como **tareas asíncronas** (*asyncrhonrous tasks*).
  También es posible definir tareas asíncronas mediante funciones `async`.
  En otro caso, se conocen como **tareas síncronas** (*synchronous tasks*).
- `log` (function). Función a través de la cual escribir mensajes de *log* en la consola.

Ninguno de los parámetros es obligatorio.
Y cuando se desea usarlos, se pueden indicar en cualquier orden.
Las funciones de tarea utilizan parámetros nominales, no ordinales como otros motores.
Y por lo tanto, el orden de los parámetros no alterará el producto.

A continuación, se muestra un ejemplo ilustrativo mediante el cual se crea una tarea simple para la supresión de un archivo local:

```
//imports
const {cat, simple} = require("ethron");
...

//definición de la tarea rm
rm = simple("rm", function(params) {
  fsx.removeSync(params.path);
});
```

Adicionalmente, se puede utilizar varios métodos de la tarea para definir algunas de sus propiedades, todos ellos devolviendo la propia tarea para facilitar su encadenamiento:

- `fmt(function) : Task`. Indica la función con la que obtener dinámicamente el título de una invocación a partir de los parámetros pasados por el usuario: `function(params) : string`.
- `title(string) : Task`. Establece el título predeterminado de la tarea.
- `desc(string) : Task`. Establece la descripción de la tarea.
- `tags(string[]) : Task`. Establece las etiquetas de la tarea.

He aquí un ejemplo ilustrativo:

```
//usando el parámetro props para definir las propiedades
rm = simple(
  {
    id: "rm",
    desc: "Remove a file or a folder.",
    fmt: (params) => "Remove " + params.path
  },

  function(params) {
    fsx.removeSync(params.path);
  }
);

//usando métodos para definir las propiedades
rm = simple("rm", function() {
  fsx.removeSync(params.path);
}).desc(
  "Remove a file or folder."
).fmt(function(params) {
  return "Remove " + params.path;
});
```

## Tareas simples asíncronas

Las tareas simples se clasifican en síncronas o asíncronas.
De manera predeterminada, son síncronas.
Para definirlas como asíncronas, no hay más que indicar el parámetro `done` en la función de tarea o usar una función `async`.

En una tarea síncrona, cuando la función de tarea finaliza, lo hace también su trabajo asociado.
En cambio, en una asíncrona, la tarea no finaliza con la función, sino cuando ha finalizado su trabajo asíncrono.
Por esta razón, el motor de tareas se queda esperando hasta que la función de tarea le indique que ha finalizado.
Para este fin, podemos usar una función `async`.
Cuando el motor se encuentra con una tarea simple cuya función de tarea se ha definido como `async`, esperará a que finalice la promesa asociada:

```
rm = simple("rm", async function(params) {
  await(remove(params.path));
});
```

También es posible indicar el parámetro `done`, una función que debe ejecutar la función de tarea para notificar que ha terminado.
Cuando se invoca `done`, se hace dos cosas:

- Por un lado, indicarle al motor de ejecución que la tarea ha terminado.
- Por otro lado, cómo ha finalizado la tarea.

Presenta las siguientes sobrecargas:

```
done()                //he finalizado bien y no devuelvo nada
done(error)           //he finalizado con el error indicado
done(null, resultado) //he finalizado bien y he aquí el valor a devolver
```

Ejemplo:

```
rm = simple("rm", function(params, done) {
  fsx.remove(params.path, done);
});
```
