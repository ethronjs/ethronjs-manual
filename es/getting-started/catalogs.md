# Catálogos

*Tiempo estimado de lectura: 12min*

Un **catálogo** (*catalog*) consiste en un contenedor de tareas invocables directamente desde la línea de comandos.
Es uno de los medios a través de los cuales se puede extender la funcionalidad de **Ethron.js**.

## Archivo de catálogo

Todo catálogo se define mediante un **archivo de catálogo** (*catalog file*), un archivo JavaScript que define las tareas que contiene el catálogo.
Generalmente, estas tareas vienen definidas mediante *plugins*, las cuales expone el catálogo, en muchas ocasiones, con determinados parámetros.

Nada mejor que un ejemplo para ir abriendo boca.
El siguiente catálogo tiene como objeto definir tareas específicas para ejecutar [Verdaccio](https://verdaccio.org) localmente, un registro NPM (por favor, échele un vistazo detenidamente):

```
//imports
const {cat} = require("ethron");
const docker = require("@ethronjs/plugin.docker");
const eslint = require("@ethronjs/plugin.eslint");
const exec = require("@ethronjs/plugin.exec");
const fs = require("@ethronjs/plugin.fs");
const verdaccio = require("@ethronjs/plugin.verdaccio");
const xdg = require("@ethronjs/plugin.xdg");
const path = require("path");
const fsx = require("fs-extra");

//Verdaccio container home.
const home = path.join(process.env.HOME, "opt/verdaccio");

//Image to use.
const image = "verdaccio/verdaccio";

//Container name.
const container = "verdaccio";

//Use sudo?
const sudo = true;

//Time to wait.
const sleep = "2s";

//Config file to use.
const config = (
  fsx.existsSync(path.join(process.cwd(), "config.yaml")) ?
    path.join(process.cwd(), "config.yaml") :
    path.join(__dirname, "tmpl/config.yaml")
);

//catalog
cat.macro("lint", [
  [eslint, "."]
]).title("Check source code");

cat.call("console", xdg.open, {
  target: "http://localhost:4873"
}).title("Open Web console");

cat.call("pull", docker.pull, {
  image
}).title("Pull Verdaccio image");

cat.macro("run", [
  [exec, `sudo rm -rf ${home}`],
  [fs.mkdir, home],
  [fs.cp, config, path.join(home, "conf", "config.yaml")],
  [fs.mkdir, path.join(home, "storage")],
  [fs.mkdir, path.join(home, "plugins")],
  [exec, `sudo chown -R 100:101 ${home}`],
  [verdaccio.run, {home, image, container, publish: 4873, sudo, sleep}],
  cat.get("console")
]).title("Run Verdaccio container");

cat.macro("start", [
  [verdaccio.start, {container, sudo, sleep}],
  cat.get("console")
]).title("Start Verdaccio container");

cat.call("stop", verdaccio.stop, {
  container,
  sudo
}).title("Stop Verdaccio container");

cat.call("rm", docker.rm, {
  container,
}).title("Remove Verdaccio container");

cat.macro("dflt", [
  cat.get("start")
]).title("Start Verdaccio container");
```

La idea del catálogo es proporcionar varias tareas relacionadas con algún aspecto.
En nuestro caso, con la instalación y ejecución de *Verdaccio*.

En primer lugar, lo que se hace es importar `ethron`, el paquete que contiene el *kernel* de **Ethron.js**, así como cualquier *plugin* necesario.
En nuestro caso, tenemos:

```
//imports
const {cat} = require("ethron");
const docker = require("@ethronjs/plugin.docker");
const eslint = require("@ethronjs/plugin.eslint");
const exec = require("@ethronjs/plugin.exec");
const fs = require("@ethronjs/plugin.fs");
const verdaccio = require("@ethronjs/plugin.verdaccio");
const xdg = require("@ethronjs/plugin.xdg");
const path = require("path");
const fsx = require("fs-extra");
```

Siempre se debe importar `ethron` en primer lugar.
De este paquete, nos quedaremos con su objeto `cat` que representa el catálogo actual.
El resto de paquetes son los *plugins* y cualquier otro paquete usado en el catálogo.

A continuación, se suele definir variables a usar a lo largo del catálogo como, p. ej.:

```
//Verdaccio container home.
const home = path.join(process.env.HOME, "opt/verdaccio");

//Image to use.
const image = "verdaccio/verdaccio";

//Container name.
const container = "verdaccio";

//Use sudo?
const sudo = true;

//Time to wait.
const sleep = "2s";

//Config file to use.
const config = (
  fsx.existsSync(path.join(process.cwd(), "config.yaml")) ?
    path.join(process.cwd(), "config.yaml") :
    path.join(__dirname, "tmpl/config.yaml")
);
```

Y finalmente, usamos el objeto `cat` para definir sus tareas.
Más adelante, hablaremos de las tareas más detenidamente, ahora nos centramos sólo en cómo definir tareas en el catálogo.
P.ej., una macro es una secuencia de tareas y se define mediante `cat.macro()`:

```
cat.macro("run", [
  [exec, `sudo rm -rf ${home}`],
  [fs.mkdir, home],
  [fs.cp, config, path.join(home, "conf", "config.yaml")],
  [fs.mkdir, path.join(home, "storage")],
  [fs.mkdir, path.join(home, "plugins")],
  [exec, `sudo chown -R 100:101 ${home}`],
  [verdaccio.run, {home, image, container, publish: 4873, sudo, sleep}],
  cat.get("console")
]).title("Run Verdaccio container");
```

En primer lugar, se indica el nombre de la tarea del catálogo, en nuestro caso, `run`.
A continuación, se define las distintas tareas asociadas a la macro mediante una lista o *array*.
Cada elemento representa una tarea a ejecutar.
Así, p. ej., `[fs.mkdir, home]` lo que dice es: crea el directorio indicado por la variable `home`.
Con `cat.get()` se obtiene una tarea ya registrada en el catálogo para su uso en una tarea compuesta.

Entre los métodos del objeto `cat` con los que crear/registrar tareas en el catálogo, tenemos:

- `simple()` para definir una tarea simple.
- `macro()` para definir una macro.
- `workflow()` para definir un flujo de trabajo.
- `call()` para definir una tarea que invoca otra tarea ya conocida con unos determinados parámetros.

## Catálogos integrados

Básicamente, los catálogos se clasifican en catálogos integrados y catálogos NPM.

Un **catálogo integrado** (*embedded catalog*) es aquel que se asocia a un determinado proyecto.
Un proyecto puede tener uno o más catálogos integrados.
Este tipo de catálogo consiste en un archivo de catálogo con un nombre con el siguiente formato:

```
Ethron.nombreCatálogo.js
```

### Definición de un archivo de catálogo integrado

Aunque más adelante presentaremos los generadores más detenidamente, vamos a presentarlos formalmente en este punto.
Un **generador** (*generator*) es un motor de tareas específico que se usa para generar archivos y/o directorios a partir de una plantilla.
Éstos se deben instalar globalmente.

Para facilitar la creación de un archivo de catálogo integrado, podemos usar el generador `@ethrongen/cat`.
El cual se instala como sigue:

```
$ npm i -g --production @ethrongen/cat
```

Una vez instalado, podemos mostrar los comandos que proporciona mediante:

```
$ ethronjs g cat -h

Command   Description                        
----------------------------------------------
dflt      Generate a NPM catalog.
embedded  Generate an embedded catalog file.
npm       Generate a NPM catalog.

$
```

Para ejecutar el comando `embedded` que genera un catálogo integrado en el directorio actual, basta con ejecutar:

```
$ ls
$ ethronjs g cat embedded
? Catalog name cat
? Would you like to generate the package.json file? No
? Would you like to generate the .eslintrc file? No


ethronjs g
----------
Generation
  Generate Ethron.cat.js OK (2 ms)

 OK       1
 FAILED   0
 TOTAL    1

 5 ms

$ ls
Ethron.cat.js
$
```

Observe que el generador le hará unas preguntas que necesita para generar los archivos necesarios.

Si el directorio actual ya contiene archivos y/o directorios, el generador no realizará la operación.
Para forzarla, indique la opción `-f`:

```
$ ethronjs g cat embedded -f
```

### Archivo package.json

Cuando un proyecto utiliza un catálogo integrado, es necesario definir también un archivo `package.json`.
Este archivo debe definir las dependencias del catálogo como, p. ej., el paquete `ethron` y los plugins usados.
Por lo general, las dependencias serán de desarrollo, pues el catálogo no es la razón de ser del proyecto.
Sino una herramienta con la que automatizar tareas frecuentemente utilizadas en el proyecto.

El generador `@ethronjs/gen.cat` permite crear el archivo `package.json`, pero si ya tiene uno, indique que no lo genere y actualícelo manualmente hasta que el generador pueda modificarlo implícitamente.

### Ejemplo de catálogo integrado

- [https://bitbucket.org/ethrongen/verdaccio/src/master/Ethron.cat.js](https://bitbucket.org/ethrongen/verdaccio/src/master/Ethron.cat.js)

## Catálogos NPM

Un **catálogo NPM** (*NPM catalog*) es aquel que se implementa mediante su propio paquete NPM.
A diferencia de un catálogo integrado que acompaña a otro paquete, un catálogo NPM es la razón de ser del paquete NPM.
El cual se publicará muy probablemente en NPM para su reutilización.

P. ej., supongamos que deseamos trabajar con *Verdaccio* para publicar nuestros paquetes privados en él.
Podemos definir un catálogo NPM con tareas varias como:

- Descargar la imagen de *Verdaccio* del *hub* de *Docker*.
- Crear un contenedor de Docker para *Verdaccio*.
- Iniciar el contenedor de Docker para *Verdaccio*.
- Detener el contenedor de Docker para *Verdaccio*.
- Etc.

### Definición de un catálogo NPM

Podemos utilizar el generador `@ethrongen/cat` para definir fácil y rápidamente los archivos y directorios de un catálogo NPM:

```
$ ethronjs g cat npm
```

### Archivo package.json

Como puede observar en el archivo `package.json` generado, en un catálogo NPM, `ethron` y los *plugins* se definen como dependencias de producción.
Recordemos que un catálogo NPM es un paquete NPM en sí mismo, el cual se puede publicar en cualquier registro NPM.

La propiedad `main` debe indicar el archivo catálogo del paquete, por convenio, `index.js`.
Actualmente, hay una restricción muy importante: debe indicar el archivo del `main` en la raíz del paquete.

### Ejemplo de catálogo NPM

- [`@ethroncat/verdaccio`](https://bitbucket.org/ethroncat/verdaccio)

## Listado de tareas de un catálogo

Se puede listar las tareas registradas en un catálogo mediante `ethronjs`.
De manera predeterminada, si no indicamos ningún comando específico, `ethronjs` lista las tareas de un catálogo.
A la hora de determinar el catálogo a usar, `ethronjs` tiene en cuenta lo siguiente:

- Si el usuario indica la opción `-c`, buscará el catálogo ahí indicado.
- Si el usuario no ha indicado la opción `-c`, buscará el catálogo indicado en la variable de entorno `ETHRONCAT`.
- Si ninguno de los dos anteriores se ha especificado, buscará el catálogo `cat`, o sea, `./Ethron.cat.js`.

Para usar el catálogo predeterminado o el indicado en la variable de entorno `ETHRONCAT`:

```
$ ethronjs
```

Para usar un catálogo NPM, indicaremos su nombre.
Ejemplo:

```
$ ethronjs -c @ethroncat/verdaccio
```

Si el catálogo tiene como prefijo `@ethroncat/` o `ethron-cat-`, no hace falta indicar el prefijo.
Bastará con:

```
$ ethronjs -c verdaccio
```

Sólo debe recordar que si va a usar un catálogo NPM, debe haberlo instalado globalmente con `npm`:

```
$ npm i -g --production @ethroncat/verdaccio
```

## Ejecución de tareas de un catálogo

Las tareas se ejectan con el comando `ethronjs r` o `ethronjs run`.
Puede usar la siguiente línea para obtener la ayuda del comando:

```
$ ethronjs r --help
```

Groso modo, para ejecutar una tarea tenemos que saber:

- El catálogo a utilizar.
  Si no es el predeterminado, indicarlo en la opción `-c` o en la variable de entorno `ETHRONCAT`.

- Los nombres de las tareas a ejecutar.

A continuación, se muestra cómo solicitar que se ejecuten las tareas `stop` y `rm` del catálogo NPM `@ethroncat/verdaccio`:

```
$ ethronjs -c verdaccio r stop rm

stop
----
Stop Verdaccio container OK (426 ms)

 OK       1
 FAILED   0
 TOTAL    1

 431 ms


rm
--
Remove Verdaccio container OK (36 ms)

 OK       1
 FAILED   0
 TOTAL    1

 36 ms

$

```

### Tarea predeterminada

De manera predeterminada, si no se indica ninguna tarea explícitamente, `ethronjs r` ejecuta la tarea cuyo nombre es `dflt`.
Esta tarea se conoce formalmente como **tarea predeterminada** (*default task*).
