# Tareas de pruebas

*Tiempo estimado de lectura: 10min*

**Ethron.js** viene de fábrica con un *framework* de pruebas de software.
Proporcionando varios tipos de tarea que tienen como objeto facilitar las pruebas de componentes de software.
De esta manera, **Ethron.js** tiene capacidades similares a otros productos dedicados a las pruebas como, por ejemplo, *Jasmine*, *Mocha* o *Jest*.
En **Ethron.js**, con la misma interfaz y forma de trabajo, podemos definir varios tipos de tarea de automatización.
Consiguiendo así una reducción considerable de la curva de aprendizaje y del número de productos a usar.
Mejorando nuestra productividad con objeto de poder dedicar más tiempo a las pruebas o a otras cosas.

Para la parte de pruebas, tenemos los siguientes tipos de tareas de prueba:

- **Suite**. Un grupo de tareas relacionadas con un mismo aspecto.
- **Prueba**. Una tarea que realiza una determinada operación de prueba.
- **Inicializador**. Una tarea simple que tiene como objeto inicializar el entorno para una o más pruebas.
- **Finalizador**. Una tarea simple que tiene como objeto poner fin a algo.

## Suites

Una **suite** es una tarea compuesta que define un grupo de tareas de prueba relacionadas con un determinado aspecto como, por ejemplo, la prueba de una determinada clase, método o función.
Es similar a los `describe()` de *Jasmine* y *Mocha*.
Está formada por:

- Cero, una o más subsuites.
- Cero, una o más pruebas.
- Cero, uno o más inicializadores.
- Cero, uno o más finalizadores.

### Definición de suites

Para definir una suite, hay que utilizar la función `suite()` del paquete `ethron`:

```
suite(props, fn) : Suite
```

El parámetro `props` es similar al de las tareas simples, las macros y los flujos de trabajo.
Mientras que el parámetro `fn` es una función *JavaScript* que define el contenido de la suite.
Esta función se conoce formalmente como **función de definición de la suite** (*suite definition function*).

Cuando una suite se define dentro de otra, ésta se asigna a su suite contenedora.

A continuación, se muestra un ejemplo ilustrativo para abrir boca:

```
//imports
import {suite, test} from "ethron";
import assert from "@ethronjs/assert";

//Test.
export default suite(__filename, function() {
  suite("from()", function() {
    suite("invalid", function() {
      test("invalid endpoint", function() {
        assert(function() {
          Nets.from(use("../data/error/conf/networks-endpoint"));
        }).raises("Invalid endpoint pattern: htp://localhost:8545.");
      })
    })

    test("valid", function(params) {
      const nets = Nets.from(require("../data/ok/conf/" + params.module));
      assert(nets).is("Nets");

      const gan = nets.get("ganache");
      assert(gan).is("GanacheNet");

      assert(gan.getAccount("faucet")).is("Account").has({
        balance: "999999999999000000000000000000",
        key: {
          pub: "0x26B9f6d4ca120aDD6aad34776C87F22988cb37f7",
          pvt: "0x120b5a263a34e333644d097aff228b74dfb4ff65e87b56b5f9f4c6589191e6d0"
        },
        password: ""
      });

      assert(gan.getAccount("deployer")).is("Account").has({
        balance: "200000000000000000000000000000",
        key: {
          pub: "0xF82E8AAf5188c5fAD8aCC8D17aCC4654a1B44444",
          pvt: "0xa10b7b6eb6b367c8f8187614158289091fc0fc150c81a01f138a2479214b45e9"
        }
        password: ""
      });

      assert(gan.getContract("One")).is("Contract").has({
        path: "test/data/ok/contracts/One",
        args: [123]
      })
    }).forEach(
      {sub: "from JS function", module: "networks-fn"},
      {sub: "from JS map", module: "networks"},
      {sub: "from JSON", module: "networks-json"}
    )
  })
})
```

## Pruebas

Una **prueba** (*test*) representa una operación que hace algo con objeto de comprobar que lo hace bien, generalmente consultando los resultados de sus acciones.

### Definición de pruebas

Se define mediante la función `test()` del paquete `ethron` y es similar a las funciones `it()` de *Jasmine*, *Mocha* y *Jest*:

```
test(props, fn) : Test
```

El parámetro `props` define las propiedades de la tarea de prueba.
Es similar al parámetro homónimo de las tareas simples y compuestas.
Y el parámetro `fn` contiene la operación que debe ejecutar el motor de tareas cada vez que se realice la prueba.
Su signatura es similar a la de las funciones de las tareas simples, pudiendo tener los parámetros nominales siguientes:

- `params` (object). Los parámetros pasados a la prueba.
  Generalmente, se pasan cuando la prueba es iterativa para cada uno de los parámetros definidos por su método `forEach()`.
- `done` (function). La función que debe invocar la función de prueba para indicar que ha terminado, si es una prueba asíncrona.
- `log` (function). La función con la que escribir en la consola.

En el ejemplo anterior, puede observar varias funciones de pruebas.

Al igual que las funciones de las tareas simples y de los flujos de trabajo, puede utilizar `async` o `done` para definir una prueba asíncrona.
A su libre elección.

## Tareas iterativas

Una **tarea iterativa** (*iterative task*) es aquella que define una secuencia de parámetros con los que ejecutar la tarea cada vez que se invoque.
Es una manera muy sencilla de ejecutar una misma acción con distintos parámetros.
Se utilizan ampliamente en las pruebas.

Para este fin, las tareas proporcionan el método `forEach()`, el cual devuelve la propia tarea para su encadenamiento:

```
forEach(...args) : Task
```

Veamos un ejemplo:

```
rm = simple("rm", function(params) {
  fsx.removeSync(params.path);
}).fmt(function(params) {
  return "Remove " + params.path;
}).forEach(
  {path: "file1.txt"},
  {path: "file2.txt"},
  {path: "file3.txt"}
);
```

Cada vez que el motor de tareas ejecute una tarea iterativa, la invocará una vez para cada uno de los parámetros indicados mediante su método `forEach()`.

Los argumentos del método `forEach()` deben de ser objetos, con los siguientes campos:

- `sub` (string). El texto a añadir al título de la invocación.
  Si no se indica, se utilizará una representación en formato JSON de `params`.
- `params` (object). Los parámetros a pasar en su invocación.

Como es muy común pasar sólo los parámetros, el método ha relajado su especificación.
De tal manera que si no se pasa un objeto, el método lo convertirá a un objeto como el siguiente: `{params: {value: valorPasado}}`.
Así pues, `forEach(123, "hola")`, es lo mismo que `forEach({params: {value: 123}}, {params: {value: "hola"}})`.

Por otra parte, si se pasa un objeto, pero sin campo `params`, el método lo convertirá a `{params: objeto}`.
Por lo tanto, `forEach({path: "file1.txt"}, {path: "file2.txt"})` es lo mismo que `forEach({params: {path: "file1.txt"}}, {params: {path: "file2.txt"}})`.

## Inicializadores

Un **inicializador** (*initializer*) es una tarea simple de prueba que tiene como objeto realizar algún tipo de preparación del entorno de prueba.
Se distingue los siguientes tipos de inicializadores:

- Los **inicializadores de suite** (*suite initializers*), los cuales se ejecutan cuando se ejecuta una suite.
  Se ejecutan una única vez para todas las subsuites y pruebas de la suite.
  Son similares a los inicializadores `beforeAll()` de otros motores.

- Los **inicializadores &ast;** (*&ast; initializers*), los cuales se ejecutan una vez cada vez que se ejecuta una prueba de la suite.
  Son similares a los inicializadores `beforeEach()` de otros motores.

- Los **inicializadores de prueba** (*test initializers*), los cuales son específicos de una determinada prueba y sólo se ejecutan cuando se ejecuta esa prueba.

- Los **inicializadores &ast;&ast;** (*&ast;&ast; initializers*), los cuales se ejecutan una vez cada vez que se ejecuta una prueba de la suite.
  Justo a continuación de los inicializadores de prueba.

Los inicializadores son ideales para realizar solicitudes de recursos o para abrir recursos como, por ejemplo, conexiones a bases de datos o a servidores.
O bien, para preparar el entorno que requiere la prueba a ejecutar.

### Definición de inicializadores

En **Ethron.js**, los inicializadores se crean con `init()`.
Por un lado, tenemos los inicializadores de suite que se definen con la función `init()` del paquete `ethron`, dentro de su suite.
La sobrecarga a utilizar es la siguiente:

```
init(fn)
```

Su función asociada es la que contiene el código que debe ejecutar el motor de tareas.
Puede ser síncrona o asíncrona, marcándose su tipo por la ausencia o presencia del parámetro `done` o el uso de `async`.

Por otro lado, tenemos los inicializadores &ast;, los cuales también se definen con la función `init()`, pero usando la siguiente sobrecarga:

```
init("*", fn)
```

Cuando el identificador de la tarea de inicialización es el asterisco (`*`), se está definiendo un inicializador &ast;.

Para los inicializadores de prueba, se utiliza el método `init()` de la prueba en cuestión:

```
test("...", function() {
  ...
}).init("titulo", fn)
```

He aquí un ejemplo:

```
suite("id de la suite", function() {
  init(function() {
    //inicializador de suite
  });

  init("*", function() {
    //inicializador *
  });

  test("id de la prueba", function() {
    //operación de prueba
  }).init("título", function() {
    //inicializador de la prueba
  }).init("título", function() {
    //otro inicializador de la prueba
  });
});
```

Una suite puede presentar tantos inicializadores como sea necesario.
Y las pruebas también.

Recordemos que los inicializadores son tareas y, por lo tanto, disponen del método `title()` con el que personalizar el título de cara al informe de resultados.
Es buena práctica asociar siempre un título a los inicializadores y finalizadores.
Ejemplo:

```
init(function() {
  nets = Nets.from(require("../../data/ok/conf/networks"));
  net = nets.get("ganache");
}).title("Read networks.js");
```

El título de un inicializador específico de prueba se pasa como primer argumento del método `init()`.

Como hemos visto, podemos definir inicializadores para todas las pruebas o para una determinada prueba.
El orden de ejecución es:

1. Los inicializadores de suite, una vez para toda la suite.
2. Los inicializadores &ast;, una vez para cada prueba de la suite.
3. Los inicializadores de prueba, una vez para la prueba que lo define.
   Aunque si estamos ante una prueba iterativa, se ejecutará una vez para cada iteración.

Qué ocurre si tenemos una prueba común que debe ejecutarse justo antes de la función de prueba, pero después de las específicas de las pruebas.
Como hemos visto, un inicializador &ast; se define mediante la función `init()`.
Mientras que un inicializador específico de una prueba, mediante su método `init()`.
El primero es común a las pruebas, mientras que el segundo no.
Ahora lo que queremos es definir un inicializador común, pero que se ejecute justo después de los específicos.
¿Es posible esto?
Sí, mediante los inicializadores &ast;&ast;, los cuales se definen con la función `init()` pero con la siguiente sobrecarga:

```
init("**", fn)
```

## Finalizadores

Un **finalizador** (*finalizer*) es la contraparte de los inicializadores y se ejecuta al final.
Y de manera similar se distingue entre finalizadores de suite, &ast;, &ast;&ast; y de prueba.

## Definición de finalizadores

Se definen igual que los inicializadores, pero mediante `fin()`, en vez de usar `init()`.

## Ejemplo

- [https://bitbucket.org/ethronjs/assert/src/master/test/unit/](https://bitbucket.org/ethronjs/assert/src/master/test/unit/)

## Ejemplo de ejecución

A continuación, se muestra la ejecución de unas pruebas para ver un ejemplo de su salida:

```
$ ethronjs

 Id.           Group  Type           Desc.                  
------------------------------------------------------------
 build                Macro          Build package          
 contracts     bc     Macro          Compile contracts      
 dflt                 Macro          Build and test         
 ganache.stop  bc     Simple()       Stop Ganache container
 lint                 Macro          Lint source code       
 pub                  Simple()       Publish on NPM         
 test                 DeferredMacro  Unit testing           

$ ethronjs r test

test
----
Unit testing
  /home/me/ethron/ethronjs/code/core/ethron-core-blockchain/dist/@ethronjs/core.blockchain/test/unit/Nets.js
    from()
      invalid
        invalid endpoint
          test(fn) OK (2 ms)
      valid ## from JS function
        test(fn) OK (8 ms)
      valid ## from JS map
        test(fn) OK (3 ms)
      valid ## from JSON
        test(fn) OK (2 ms)
  /home/me/ethron/ethronjs/code/core/ethron-core-blockchain/dist/@ethronjs/core.blockchain/test/unit/index.js
    test(fn) OK (0 ms)
  /home/me/ethron/ethronjs/code/core/ethron-core-blockchain/dist/@ethronjs/core.blockchain/test/unit/net/GanacheNet.js
    init(Read networks.js) OK (1 ms)
    start()
      lib
        test(fn) OK (1416 ms)
        fin(Stop Ganache) OK (0 ms)
      docker
        test(fn) OK (3051 ms)
        fin(Stop Ganache) OK (10501 ms)
    stop()
      from started
        test(fn) OK (0 ms)
      from stopped
        test(fn) OK (0 ms)
    restart()
      init(Start Ganache) OK (89 ms)
      test(fn) OK (64 ms)
      fin(Stop Ganache) OK (0 ms)
    deploy()
      init(Start Ganache) OK (49 ms)
      init(Get deployer address) OK (0 ms)
      non-payable contract
        with default conf
          test(fn) OK (150 ms)
        with explicit arguments
          test(fn) OK (129 ms)
      payable contract
        with default conf
          test(fn) OK (46 ms)
        with explicit arguments
          test(fn) OK (36 ms)
      fin(Stop Ganache) OK (0 ms)
    contract()
      init(Start Ganache) OK (36 ms)
      init(Deploy contract) OK (44 ms)
      existing
        test(fn) OK (13 ms)
      not existing
        test(fn) OK (0 ms)
      fin(Stop Ganache) OK (0 ms)
    transfer()
      init(Start Ganache) OK (34 ms)
      from default faucet
        test(fn) OK (34 ms)
      from specific source
        test(fn) OK (36 ms)
      fin(Stop Ganache) OK (0 ms)

 OK       31
 FAILED   0
 TOTAL    31

 16094 ms

$
```

Observe que tanto las suites como las pruebas son tareas compuestas.
El título de las funciones de prueba es siempre `fn(test)`.
El de los inicializadores: `init(título)`.
Y el de los finalizadores: `fin(título)`.
